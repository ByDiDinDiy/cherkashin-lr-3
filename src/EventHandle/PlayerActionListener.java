import java.util.EventListener;

public interface PlayerActionListener extends EventListener {
    void playerFieldClick(PlayerActionEvent e);
}
