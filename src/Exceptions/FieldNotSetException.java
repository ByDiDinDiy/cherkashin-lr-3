public class FieldNotSetException extends RuntimeException {
    public FieldNotSetException(String message) { super(message); }
}
